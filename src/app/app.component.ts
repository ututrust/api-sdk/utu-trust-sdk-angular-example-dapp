import { Component } from '@angular/core';
import '@ututrust/web-components';

// you Need to add the following line as the SDK does not it have its *.d.ts typing files yet:
// @ts-ignore
import { addressSignatureVerification, AuthData } from "@ututrust/web-components";

import { ethers } from "ethers";


import { EthereumClient, w3mConnectors, w3mProvider } from '@web3modal/ethereum'
import { Web3Modal } from '@web3modal/html'
import { configureChains, createConfig } from '@wagmi/core'
import { polygon } from '@wagmi/core/chains'
import { getAccount } from '@wagmi/core'


const chains = [polygon]
const projectId = '6133adf3bee71a90c0c5e582d52c3f12'

const { publicClient } = configureChains(chains, [w3mProvider({ projectId })])
const wagmiConfig = createConfig({
  autoConnect: true,
  connectors: w3mConnectors({ projectId, chains }),
  publicClient
})
const ethereumClient = new EthereumClient(wagmiConfig, chains)
const web3modal = new Web3Modal({ projectId }, ethereumClient)



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'utu-trust-sdk-angular-example-dapp';
  overrideApiUrl = 'https://stage-api.ututrust.com';
  walletAddress: any = "";
  hasToken = false;

  OFFERS = [
    {
      name: "Paul",
      id: "provider_1"
    },
    {
      name: "Jane",
      id: "provider_2"
    },
    {
      name: "Ali",
      id: "provider_3"
    }
  ];


  async onClickWallet() {
    web3modal.openModal()
    const account = getAccount()
    if (account.address) {
      this.walletAddress = account.address;
    }
  }

  async onConnectToUtuClick() {
    let authDataResponse = await addressSignatureVerification(this.overrideApiUrl);
    for (let i = 0; i < this.OFFERS.length; i++) {
      await this.initEntity(authDataResponse, this.OFFERS[i]);
    }
    this.triggerUtuIdentityDataSDKEvent(authDataResponse)
    this.hasToken = true;
  }


  triggerUtuIdentityDataSDKEvent(identityData: AuthData) {
    const event = new CustomEvent("utuIdentityDataReady", { detail: identityData, });
    window.dispatchEvent(event);
  };

  async initEntity(data: AuthData, offer: any) {
    await fetch(this.overrideApiUrl + "/core-api-v2/entity", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        authorization: `Bearer ${data.access_token}`,
      },
      body: JSON.stringify({
        name: offer.id,
        type: "provider",
        ids: { uuid: this.getId(offer.id) },
      }),
    })
    .then((res) => { return res; })
    .catch((err) => { console.log("Failed to init entity on utu browser extension"); });
  };


  public getId(assetIdentifier: string) {
    return ethers
      .id(assetIdentifier)
      .slice(0, 40 + 2)
      .toLowerCase()
  }

}
